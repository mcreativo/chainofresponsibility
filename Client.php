<?php




$handlerA = new HandlerA();
$handlerB = new HandlerB();

if ($request->getAttribute() == 'handerA') {
    $handlerA->handleRequest($request);
} else {
    $handlerB->handleRequest($request);
}