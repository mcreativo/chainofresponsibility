<?php


class HandlerA implements Handler
{
    public function handleRequest(Request $request)
    {
        echo 'handlerA doing something with request' . PHP_EOL;
    }
}