<?php


class HandlerB implements Handler
{

    public function handleRequest(Request $request)
    {
        echo 'handlerB doing something with request' . PHP_EOL;
    }
} 