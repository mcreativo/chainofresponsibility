<?php


interface Handler
{
    public function handleRequest(Request $request);
} 