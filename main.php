<?php

function __autoload($class_name)
{
    include $class_name . '.php';
}

$opt = getopt('', array('type:'));
$type = isset($opt['type']) ? ($opt['type'] == 'B' ? 'B' : 'A') : 'A';

$request = new Request('handler' . $type);

include 'Client.php';

