<?php


class Request
{
    /**
     * @var string
     */
    public $attribute;

    function __construct($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttribute()
    {
        return $this->attribute;
    }


} 